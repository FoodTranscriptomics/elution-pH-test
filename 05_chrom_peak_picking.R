library(purrr)
library(furrr)
library(MSnbase)
library(dplyr)
library(xcms)

# h = height
# sigma = std of distribution
# mu = mean (scan)
# egauss RMSE of fit
integrate_EICs <- function(EIC){

    peaks <- findChromPeaks(EIC, param = params$CentWave) %>%
                as_data_frame %>%
                # filter(scpos>0) %>%
                # filter(maxo==max(maxo)) %>%
                # filter(sn==max(sn)) %>%
                # slice(1) %>% # because sometimes we get the same twice...
                mutate(rt=rt/60, rtmin = rtmin/60,  rtmax = rtmax/60) %>%
                mutate(minperscan=rt/mu, FWHM = 2*sqrt(2*log(2))*sigma*minperscan) %>%
                select(rt, rtmin, rtmax, into, intb, maxo, sn, FWHM)


    if(nrow(peaks)==0) {
        peaks <- data_frame(rt = NA, rtmin = NA, rtmax = NA, into = NA, intb = NA, maxo = NA, sn = NA, FWHM = NA) %>%
                 mutate_all(funs(as.numeric))
        }

    out <- bind_cols(
                        data_frame(file_id = rep(fromFile(EIC)   , nrow(peaks)),
                                   mz_lower = rep(EIC@filterMz[1], nrow(peaks)),
                                   mz_upper = rep(EIC@filterMz[2], nrow(peaks))
                                   ),
                        peaks
                        )

    return(out)
}



plan(multiprocess, gc = TRUE)
options(future.globals.maxSize = 5 * 1024^3) # allow 5 GB for each worker


# Need because lapply/map does not take a single chromatogram but a list (column?)
EICs_list <- list()
for(i in 1:length(EICs)) EICs_list[[i]] <- EICs[[i]]

EICs_peaks <-
                future_map(EICs_list, integrate_EICs,
                           .progress = TRUE,
                           .options = future_options(globals = c("integrate_EICs", "params"), packages = c("MSnbase", "xcms", "dplyr"))
                           ) %>%
                bind_rows()


EICs_peaks <- pData(EICs) %>%
                mutate(file_id = 1:n()) %>%
                right_join(EICs_peaks, by="file_id") %>%
                mutate(polarity=ifelse(ion_mode=="negative", "NEG", "POS")) %>%
                select(-file_id, -ion_mode) %>%
                left_join(mz_intevals, by = c("polarity","mz_upper", "mz_lower"))

