library(readxl)
library(dplyr)
library(ggplot2)
library(tidyr)


level_rounds <- function(x){
    a <- x %>% filter(FA_percent=="1 %", round==1) %>% pull(into)
    b <- x %>% filter(FA_percent=="1 %", round==2) %>% pull(into)

    if(length(a)!=0 & length(b)!=0) x <- x %>% mutate(into = ifelse(round==2, into*(a/b), into))

    if(length(a)!=0) x <- x %>% filter(!(FA_percent=="1 %" & round==2))

    return(x)
}




data_pH <- read_excel("input/pH_EIC.tab_new_transfer_SA.xlsx") %>%
           filter(`To keep`==1)


data_pH <- data_pH %>%
            filter(!(filename=="180131_Std_POS_1FA_1.mzML" & Name=="trans-Piceid")) %>%
            nest(-compound, -polarity) %>%
            mutate(data = map(data,level_rounds)) %>%
            unnest(data)





# Plot_fun ----------------------------------------------------------------
pH_plot <- function(x){
x %>%
        mutate(FA_percent = factor(FA_percent, c("5 %", "2.5 %", "1 %", "ACN 1 % FA", "0.1 %", "pH 4", "pH 5.8","pH 9.2"))) %>%
        mutate(FA_percent = droplevels(FA_percent)) %>%
        select(Name, FA_percent, into, FWHM, polarity, rt) %>%
        complete(Name, FA_percent, polarity) %>%
        gather("key", "value", -Name, -FA_percent, -polarity) %>%
        mutate(polarity = ifelse(polarity=="POS", "Positive mode", "Negative mode")) %>%
        mutate(key = ifelse(key=="into", "Area",key)) %>%
        mutate(key = ifelse(key=="rt", "Retention time",key)) %>%
        mutate(key = ifelse(key=="FWHM", "Peak width (min)",key)) %>%

        mutate(FA_percent = recode(FA_percent,
                                   `0.1 %` = "2.7\n\n0.1% FA\nMeOH",
                                   `1 %` = "2.2\n\n1% FA\nMeOH",
                                   `ACN 1 % FA` = "2.2\n\n 1% FA\nAcN",
                                   `2.5 %` = "2\n\n2.5% FA\nMeOH",
                                   `5 %` = "1.8\n\n5% FA\nMeOH",
                                   `pH 4` = "4\n\nOAc−\nMeOH",
                                   `pH 5.8` = "5.8\n\nOAc−\nMeOH",
                                   `pH 9.2` = "9.2\n\nAF\nMeOH"
                                  )
              ) %>%

            {
                ggplot(data=., aes(FA_percent, value, fill=Name)) +
                geom_bar(stat="identity", position="dodge") +
                theme_classic() +
                facet_grid(key~polarity, scale="free_y", switch="y") +
                theme(strip.text = element_text(face="bold", size=9),
                      strip.background = element_rect(fill="lightgrey", colour="black",size=1)) +
                theme(plot.title = element_text(hjust = 0.5, size = 20, face = "bold"),
                      axis.title = element_text(size = 18, face = "bold"),
                      axis.text  = element_text(size = 12)
                      ) +
                labs(x = "pH", y="", fill="") +
                theme(legend.position="top")
            }
}

# Anthocyanins ------------------------------------------------------------
p_ant <- data_pH %>%
            filter(Name %in% c("Cyanidin-3-O-glucoside", "Malvidin-3-O-glucoside", "Peonidin-3-O-glucoside", "Delphinidin-3-O-glucoside", "Petunidin-3-O-glucoside")) %>%
            mutate(Name = factor(Name, c("Cyanidin-3-O-glucoside", "Peonidin-3-O-glucoside", "Petunidin-3-O-glucoside", "Delphinidin-3-O-glucoside", "Malvidin-3-O-glucoside"))) %>%
            {
                pH_plot(.) +
                scale_fill_brewer(type = "seq", palette = "Reds") +
                ggtitle("Anthocyanins")
            }

g <- ggplot_build(p_ant)
g2 <- ggplot_build(p_ant+scale_y_continuous(breaks=4:30, limits=c(4.5,10)))
g$layout$panel_ranges[[5]] <- g2$layout$panel_ranges[[5]]
g$layout$panel_ranges[[6]] <- g2$layout$panel_ranges[[6]]
g <- ggplot_gtable(g)
plot(g)
ggsave("plots/Anthocyanins.pdf", g, width = 16, height = 12, scale=0.8)


# Flavan-3-ols ------------------------------------------------------------
p_fla <- data_pH %>%
            filter(Name %in% c("Epigallocatechin gallate", "Procyanidin B2", "(+)-Catechin", "(-)-Epicatechin")) %>%
            mutate(Name = factor(Name, c("Epigallocatechin gallate", "Procyanidin B2", "(-)-Epicatechin", "(+)-Catechin"))) %>%
            {
                pH_plot(.) +
                scale_fill_brewer(type = "seq", palette = "Blues") +
                ggtitle("Flavan-3-ols")
            }


ggsave("plots/Flavan-3-ols.pdf", p_fla, width = 16, height = 12, scale=0.8)



# Flavanones --------------------------------------------------------------
p_flavanones <- data_pH %>%
            filter(Name %in% c("(±)-Naringenin", "Naringenin-7-O-glucoside")) %>%
            mutate(Name = factor(Name, c("(±)-Naringenin", "Naringenin-7-O-glucoside"))) %>%
            {
                pH_plot(.) +
                scale_fill_brewer(type = "seq", palette = "Blues") +
                ggtitle("Flavanones")
            }


ggsave("plots/Flavanones.pdf", p_flavanones, width = 16, height = 12, scale=0.8)



# Dihydrochalcones --------------------------------------------------------
p_Dihydrochalcones <- data_pH %>%
            filter(Name %in% c("Phloretin")) %>%
            mutate(Name = factor(Name, c("Phloretin"))) %>%
            {
                pH_plot(.) +
                scale_fill_brewer(type = "seq", palette = "Blues") +
                ggtitle("Flavanones")
            }


ggsave("plots/Dihydrochalcones.pdf", p_Dihydrochalcones, width = 16, height = 12, scale=0.8)


# Stilbenes ---------------------------------------------------------------
p_Stilbenes <- data_pH %>%
            filter(Name %in% c("trans-Piceid", "trans-Resveratrol")) %>%
            mutate(Name = factor(Name, c("trans-Piceid", "trans-Resveratrol"))) %>%
            {
                pH_plot(.) +
                scale_fill_brewer(type = "seq", palette = "Blues") +
                ggtitle("Stilbenes")
            }


ggsave("plots/Stilbenes.pdf", p_Stilbenes, width = 16, height = 12, scale=0.8)


# Hydroxycinnamic acid ----------------------------------------------------
p_hydro_cin <- data_pH %>%
            filter(Name %in% c("Caftaric Acid", "trans-cinnamic acid", "p-Coumaric acid", "Caffeic Acid", "Ferulic Acid", "Sinapic Acid")) %>%
            mutate(Name = factor(Name, c("Caftaric Acid", "trans-cinnamic acid", "p-Coumaric acid", "Caffeic Acid", "Ferulic Acid", "Sinapic Acid"))) %>%
            {
                pH_plot(.) +
                scale_fill_brewer(type = "seq", palette = "Blues") +
                ggtitle("Hydroxycinnamic acids")
            }


ggsave("plots/Hydroxycinnamic_acids.pdf", p_hydro_cin, width = 16, height = 12, scale=0.8)


# Hydroxybenzoic acids derivatives ----------------------------------------
p_hydroben <- data_pH %>%
            filter(Name %in% c("4-Hydroxybenzoic acid", "Vanillic Acid", "Gallic Acid", "syringic acid", "3,4-Dihydroxybenzoic acid")) %>%
            mutate(Name = factor(Name, c("4-Hydroxybenzoic acid", "Vanillic Acid", "Gallic Acid", "syringic acid", "3,4-Dihydroxybenzoic acid"))) %>%
            {
                pH_plot(.) +
                scale_fill_brewer(type = "seq", palette = "Blues") +
                ggtitle("Hydroxybenzoic acids derivatives")
            }

ggsave("plots/hydroben.pdf", p_hydroben, width = 16, height = 12, scale=0.8)


# Benzoic acid derivatives ------------------------------------------------
p_benzo <- data_pH %>%
            filter(Name %in% c("Pyrogallol", "Ellagic acid")) %>%
            mutate(Name = factor(Name, c("Pyrogallol", "Ellagic acid"))) %>%
            {
                pH_plot(.) +
                scale_fill_brewer(type = "seq", palette = "Blues") +
                ggtitle("Benzoic acid derivatives")
            }


ggsave("plots/benzo.pdf", p_benzo, width = 16, height = 12, scale=0.8)






# Mixed ------------------------------------------------
p_phenolics <- data_pH %>%
            filter(Name %in% c("Quercetin", "(+)-Catechin", "Procyanidin B2", "trans-Piceid")) %>%
            mutate(Name = factor(Name, c("Quercetin", "(+)-Catechin", "Procyanidin B2", "trans-Piceid"))) %>%
            {
                pH_plot(.) +
                scale_fill_brewer(type = "seq", palette = "Greens") +
                ggtitle("Selected flavonoids")
            }


ggsave("plots/flavonoids.pdf", p_phenolics, width = 16, height = 12, scale=0.8)




p_others <- data_pH %>%
            filter(Name %in% c("Gallic Acid", "syringic acid", "Caffeic Acid", "Sinapic Acid", "Ellagic acid")) %>%
            mutate(Name = factor(Name, c("Gallic Acid", "syringic acid", "Caffeic Acid", "Sinapic Acid", "Ellagic acid"))) %>%
            {
                pH_plot(.) +
                scale_fill_brewer(type = "seq", palette = "Blues") +
                ggtitle("Other phenolics")
            }


ggsave("plots/others.pdf", p_others, width = 16, height = 12, scale=0.8)

