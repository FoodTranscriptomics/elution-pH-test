# Libraries ---------------------------------------------------------------
library(googlesheets)
library(MSnbase)
library(dplyr)
library(ggforce)
library(rinchi)
library(rcdk)
library(purrr)
library(tidyr)

source("FUNS.R")


# Funs --------------------------------------------------------------------
do.typing_p <- possibly(do.typing, NA)
do.aromaticity_p <- possibly(do.aromaticity, NA)
do.isotopes_p <-  possibly(do.isotopes, NA)

mol_2_exact_mass <- function(x){
    do.typing_p(x)
    do.aromaticity_p(x)
    do.isotopes_p(x)

    mass <- get.exact.mass(x)

    return(mass)
}



# Read google sheet -------------------------------------------------------
gdoc <- gs_title("wine_compounds", verbose = TRUE)
data <- gs_read(gdoc, ws = "Data_extended")



# Make chromatograms for both stds and wine -------------------------------

pheno <- fData(raw) %>%
            select(fileIdx , polarity) %>%
            distinct %>%
            right_join(pData(raw) %>% mutate(fileIdx = 1:n()), by = "fileIdx") %>%
            as_tibble %>%
            mutate(polarity = c("NEG", "POS")[polarity+1]) %>%
            rename(filename_factor = filename) %>%
            mutate(file=basename(filepath))



# Make intervals ----------------------------------------------------------
ppm <- 2000 # +/-


mz_intevals <- data %>%
                select(Name, `[M-H]-`, `[M+H]+`) %>%
                gather("polarity", "mz", -Name) %>%
                mutate(polarity = ifelse(polarity=="[M-H]-", "NEG", "POS")) %>%
                mutate(mz_upper = mz + ppm*mz*1E-6, mz_lower = mz - ppm*mz*1E-6)

mz_intevals_mat <- mz_intevals %>%
        select(mz_lower, mz_upper) %>%
        as.matrix


# Make chromatograms ------------------------------------------------------
EICs <- chromatogram(raw, mz = mz_intevals_mat, aggregationFun = "sum", missing = 0)


EICs_long <- .ChromatogramsLongFormat(EICs, pdata = new("NAnnotatedDataFrame", pheno)) %>%
                as_tibble %>%
                rename(polarity_file = polarity)


EICs_long <- mz_intevals %>%
                mutate(interval_name = Name, int_idx = 1:n()) %>%
                select(interval_name, int_idx, polarity) %>%
                mutate(interval_name = factor(interval_name, levels = unique(interval_name))) %>%
                right_join(EICs_long, by = "int_idx") %>%
                rename(polarity_interval = polarity) %>%
                filter(polarity_interval == polarity_file) %>%
                mutate_if(is.factor, droplevels)




